Rails.application.routes.draw do
  resources :users
  #root to:'users#index'
  get 'profile/profile'
  get 'post/post'
  get 'about/about'
  get 'home/home'
  get 'found/found'
  get 'find/find'
  get 'signup/signup'
  get 'lost/lost' 
  get 'welcome/index'
  root 'home#home'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
