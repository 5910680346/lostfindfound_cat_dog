Feature: Manage users
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new user
    Given I am on the new user page
    When I fill in "Name" with "name 1"
    And I fill in "Surname" with "surname 1"
    And I fill in "Id card" with "id_card 1"
    And I fill in "Phone" with "phone 1"
    And I fill in "Username" with "username 1"
    And I fill in "Password" with "password 1"
    And I press "Save"
    Then I see the home page
#    And I should see "surname 1"
#    And I should see "id_card 1"
#    And I should see "phone 1"
#    And I should see "username 1"
#    And I should see "password 1"

#  Scenario: Delete user
#     Given the following users:
#      |name|surname|id_card|phone|username|password|
#      |name 1|surname 1|id_card 1|phone 1|username 1|password 1|
#      |name 2|surname 2|id_card 2|phone 2|username 2|password 2|
#      |name 3|surname 3|id_card 3|phone 3|username 3|password 3|
#      |name 4|surname 4|id_card 4|phone 4|username 4|password 4|
#    When I delete the 3rd user
#    Then I should see the following users:
#      |Name|Surname|Id card|Phone|Username|Password||||
#      |name 1|surname 1|id_card 1|phone 1|username 1|password 1|Show|Edit|Destroy|
#      |name 2|surname 2|id_card 2|phone 2|username 2|password 2|Show|Edit|Destroy|
#      |name 4|surname 4|id_card 4|phone 4|username 4|password 4|Show|Edit|Destroy|
