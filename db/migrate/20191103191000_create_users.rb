class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.string :id_card
      t.string :phone
      t.string :username
      t.string :password

      t.timestamps
    end
  end
end
